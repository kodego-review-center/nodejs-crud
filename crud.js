const http = require ("http");
const PORT = 4000;

//mock data
let directory = [

    {
        name: "Jericho Bongay",
        email: "jericho@mail.com"
    },

    {
        name: "Regienald Almoite",
        email: "regie@mail.com"
    }

];

http.createServer( (req, res) => {

    //endpoint /users
    //method "GET"
    //response should return data from mock data
    if (req.url === "/users" && req.method === "GET")
    {
        res.writeHead(200, {"Content-Type": "application/json"});
        res.write(JSON.stringify(directory));
        res.end();
    }

    //endpoint /users
    //method is POST
    //response is to return the data that is being inserted in our mock data array 
    //however, we must insert the data first in the mock array
    if (req.url === "/users" && req.method === "POST")
    {
        let reqBody = "";

        req.on("data", (data) => {
            reqBody += data
        });

        req.on("end", () => {

            //parse reqBody to JavaScript object so that we can insert it to the mock database. 
            reqBody= JSON.parse(reqBody);

            //will insert object in our array of objects in our mock data
            directory.push(reqBody);

            //now that we successfully added our object into the array we will now send a response back to the client.
            res.writeHead(200, {"Content-Type": "application/json"});
            res.write(JSON.stringify(directory));
            res.end();
        });
    }

}).listen(PORT, () => console.log(`Server running at port ${PORT}`));