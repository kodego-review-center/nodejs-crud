const http = require ("http");
const port = 4000;

http.createServer((request, response) => {
    //statement
    // console.log ("hello");
    // console.log (request);


    //endpoint - /profile
    //http method - GET
    //use the comparison and logical operators in if condition
    if (request.url == "/profile" && request.method == "GET") {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Welcome to my page");
    }

    else if (request.url == "/profile" && request.method == "POST") {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Data to be send to the database");
    }

    else{
        response.writeHead (404, {"Content-Type": "text/plain"});
        response.end("Request cannot be completed");
    }
}).listen(port);

//Step 1: open your terminal then type nodemon script.js
//Step 2: Open your postman, create a collection, add request, choose GET method, then type http://localhost:4000/profile